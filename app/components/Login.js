import React from "react";
import { Redirect } from "react-router-dom";

class Login extends React.Component {
    constructor() {
        super();
        this.state = {
            redirect: false,
            logUserId: ''
        }

        // this.login = this.login.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.setState({
            logUserId: e.target.value
        })
    }

    handleLogin(e) {
        e.preventDefault();
        this.props.authenticate(this.state.logUserId).then(status => this.setState({
            redirect: status
        })).catch(() => this.setState({
            redirect: false
        }));
    }

    render() {
        const backURL = location.search.substring(location.search.search('back=') + 5);

        if (this.state.redirect) {
            return <Redirect to={{
                pathname: '/goback',
                state: {
                    auth: true,
                    backURL: backURL
                }
            }} />;
        }

        return (
            <div>
                <p> You must log in to view the page at {backURL} </p>
                <form onSubmit={this.handleLogin}>
                    <input
                        onChange={this.handleChange}
                        value={this.state.logUserId}
                        placeholder="Type your chat ID"
                        type="text"
                    />
                    <button type="submit"> Log in </button>
                </form>
            </div>
        );
    }
}
export default Login;
