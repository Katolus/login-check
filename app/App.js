import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";

import Login from "./components/Login";

const allowedUsers = [{ login: 'Katolus' }];

class App extends React.Component {

    constructor() {
        super();
        this.state = {
            // isAuthenticated: localStorage.getItem('isGit') === 'true' ? true : false
            isAuthenticated: false
        }
        this.authenticate = this.authenticate.bind(this);
        this.signOut = this.signOut.bind(this);

    }
    authenticate(userId) {
        const App = this;
        console.log(`Logging with ${userId} user id.`)
        // getUserAuthorization(userId);
        return new Promise(function (resolve, reject) {
            if (allowedUsers.filter(user => user.login === userId).length === 1) {
                App.setState({
                    isAuthenticated: true
                });
                // localStorage.setItem('isGit', true);
                resolve(true);
            } else {
                console.log('Unable to authenticate the user with id ' + userId);
                reject(false);
            }
        });
    }

    signOut() {
        this.setState({
            isAuthenticated: false
        });
        localStorage.removeItem('isGit');
        // setTimeout(cb, 100);
    }

    componentDidMount() {
        if (localStorage.getItem('isGit') === 'true') {
            console.log('Accessing localStorage');
            this.setState({
                isAuthenticated: true
            });
        }
        // console.log('Component mounted!');
    }

    componentWillUnmount() {
        console.log('Login will unmount!');
    }

    render() {
        return (
            <Router>
                <Switch>
                    <Route exact path="/login" render={(props) =>
                        <Login
                            authenticate={this.authenticate}
                            {...props}
                        />
                    } />
                    <Route exact path="/goback" render={(props) => (
                        location.assign('http://' + props.location.state.backURL + '/?auth=true'),
                        (
                            <div>...Redirecting...</div>
                        )
                    )
                    } />
                </Switch>
            </Router >
        )
    }
}

export default App;